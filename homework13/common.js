
const images = document.querySelectorAll('.image-to-show');
const imagesWrapper = document.querySelector('.images-wrapper');

let currentIndex = 0;
let intervalId = null;

function changeImage() {
  images[currentIndex].classList.remove('active');
  currentIndex = (currentIndex + 1) % images.length;
  images[currentIndex].classList.add('active');
}

images[currentIndex].classList.add('active');
if (!intervalId) {
    intervalId = setInterval(changeImage, 3000);
  }

const stopButton = document.createElement('button');
stopButton.textContent = 'Припинити';
stopButton.style.width = '80px'
stopButton.style.height = '70px'
stopButton.addEventListener('click', ()=>{
    clearInterval(intervalId);
  intervalId = null;
});

const resumeButton = document.createElement('button');
resumeButton.textContent = 'Відновити показ';
resumeButton.style.width = '80px'
resumeButton.style.height = '70px'
resumeButton.addEventListener('click', ()=>{
    images[currentIndex].classList.add('active');
    if (!intervalId) {
        intervalId = setInterval(changeImage, 3000);
      }
});

imagesWrapper.append(stopButton);
imagesWrapper.append(resumeButton);


