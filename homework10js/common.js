"use strict";

const tab = document.querySelectorAll(".tabs-title");
const text = document.querySelectorAll(".text")

tab.forEach((item)=>{
    item.addEventListener("click", ()=>{
    let getAtribute = item.dataset.tab // беремо data-атрибути
    let getText = document.querySelector(getAtribute) // перебираємо data-атрибути

        tab.forEach((item)=>{
            item.classList.remove("active") // прибираємо клас .active
        })
        text.forEach((item)=>{
            item.classList.remove("active")
        })


        item.classList.add("active") // додаємо клас .active
        getText.classList.add("active")
    })
})
