"use strict";

const drawCircleBtn = document.getElementById('CircleBtn');
drawCircleBtn.addEventListener('click', ()=>{

  const diameterInput = document.createElement('input');
  diameterInput.type = 'number';
  diameterInput.placeholder = 'Введіть діаметр';
  const drawBtn = document.createElement('button');
  drawBtn.innerText = 'Намалювати';

  document.body.append(diameterInput);
  document.body.append(drawBtn);
  
  drawBtn.addEventListener('click', ()=>{
    const diameter = diameterInput.value;
      for (let i = 0; i <= 100; i++) {
        console.log(i)
        const circle = document.createElement('div');
        circle.classList.add('circle');
        circle.style.width = `${diameter}px`;
        circle.style.height = `${diameter}px`;
        circle.classList.add("circle")
        circle.style.backgroundColor = getRandomColor();
        circle.addEventListener('click',(event)=>{
          event.target.style.display = "none"
        });
        document.body.append(circle);
      } 
    })
  });

  function getRandomColor() { 
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
  
    return `rgb(${r}, ${g}, ${b})`;
  }
  



