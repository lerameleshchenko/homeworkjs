const bodyElement = document.querySelector("body");

const button = document.querySelector(".button");

button.addEventListener("click", remove)

function addThemes(){ // зміна класу 
if(localStorage.getItem("theme") === "dark"){
 bodyElement.classList.add("dark");
}else{
    bodyElement.classList.remove("dark");
}
}

function remove(){ // перевірка чи є ця тема в localStorage
    if(localStorage.getItem('theme') === 'dark'){
        localStorage.removeItem('theme');
    }else{
        localStorage.setItem("theme", "dark");
    }
    addThemes()
} 
addThemes()